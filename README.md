# 2021-10-28 Pull Request 门禁检查帮助文档


# 功能介绍

Pull Request 门禁检查功能提供了在代码更改时为`代码仓库`集成自动化的能力。门禁检查能够让你通过构建一个 Gitee 第三方应用程序，在仓库 Pull Request 代码产生变更时，集成执行持续集成、代码分析、代码检查扫描、自动部署等服务。并将相应的执行结果返回给到 Pull Reequest 提交者。






# 背景

不知道开发者们是否有这样的诉求，在创建 Pull Request 后，希望通过自动化检测服务能代替开发者自动做一部分的代码评审，如：

- CLA/DCO 签署
- 编程语言规范
- 编译测试
- 静态检查
 ……

自动化检测完成后，通过 API 手段向 Pull Request 评论区输出检测报告，并且在 Pull Request 标签上添加检测结果，如：`CLA检查成功` `静态检查成功` `编译成功`等，用于作为 Pull Request 合并的依据。有些同学一定觉得这个过程太不友好了：

- 检测报告太多，污染评论区，影响代码评审
- 检测报告没有代码行间注释
- 检测以标签展示，起不到合并卡位的作用
- 检测的触发和重新触发都需要通过评论区触发
- 检测报告无法与具体提交的 commit 代码版本一一对应
- 相同检测服务不能形成标准化服务，推广给其他组织、用户使用
 ……

那么，有没有一种方法可以解决上面的问题呢？
答案是：**有！**

### Pull Request 检查功能介绍

现在 Gitee 推出了 Pull Request **检查**功能，提供了以下能力：
1、Pull Request 流程能对接外部服务
2、Gitee 页面上能彰显外部服务的状态和结果，并支持界面交互直接调用外部服务提供的能力
3、提供 Pull Request 门禁能力

这里使用一个简单时序图来解释下如何接入外部服务能力
![](https://images.gitee.com/uploads/images/2021/0901/175609_cec4deeb_1841492.png "屏幕截图.png")
如上图所示，Gitee 推送事件并接受用户操作请求以完成对接。即 Gitee 需要提供 WebHook 事件通知和接受用户操作检测请求的 API 接口。

### 接口说明

> PS：调用检查接口，需要拥有仓库管理员的权限

#### 创建一个检查任务

> `POST` : /repos/{owner}/{repo}/check-runs

| 参数名 | 数据类型 | 参数所在位置 | 参数描述 |
|---|---|---|---|
| owner | string | path | 仓库所在的空间地址，以 https://gitee.com/oschina/git-osc 为例，`repo` 取值为 oschina |
| repo | string | path | 仓库地址，以 https://gitee.com/oschina/git-osc 为例，`repo` 取值为 git-osc |
| name | string | formData | 必填参数，检查任务名称，如：`ci-pipeline-runn` |
| head_sha | string | formData | 必填参数，表示当次代码推送时，分支上最新提交的 Commit SHA |
| details_url | string | formData | 选填，提供门禁检查项能力服务主页 **诺墨这里改一下** |
| status | string | formData | 选填，当前检查任务所处的状态。具体值可以是以下值的其中一个：<br> - `queued`: 表示当前检查任务处于排队中 (默认值) <br>- `in_progress`: 表示当前检查任务处于进行中状态<br> - `completed`: 表示当前检查任务已执行完毕|
| started_at | string | formData | 选填，检查任务的开始时间，要求时间格式符合 `ISO 8601` 规范 |
| completed_at | string | formData | 选填，检查任务的完成时间，要求时间格式为 `ISO 8601` 规范 |
| conclusion | string | formData | 选填，检查任务的最终状态（结论），具体值可以是以下值的其中一个：<br> - `action_required`: 待解决 <br>- `cancelled`: 已取消 <br>- `failure`: 任务失败 <br>- `neutral`: 暂无状态 <br>- `success`: 任务成功 <br>- `skipped`: 跳过 <br>- `stale`: 陈旧的 <br>- `timed_out`: 任务超时 <br>当检查任务结束时（`completed_at` 不为空或 `status` 取值为 `completed`），`conclusion` 必填参数。<br>当前字段取值为 `action_required` 时，应在 `details_url` 提供详情地址。<br>当任务结束，提供了结论将自动设置该检查任务为 `completed` 状态。将再也无法更改该结论 |
| output | object | formData | 选填，检查任务结束后提供的检测报告，具体数据结构参照下方 **《门禁检查报告数据结构》** |
| actions | objects | formData | 选填，在门禁检查相关界面上显示一个按钮，点击提醒你的做额外的事情 **诺墨这里改一下** |


##### 门禁检查结果报告()数据结构

门禁检查的报告通过在 `门禁检查任务` 中指定 `output` 字段，定义并输出具体的报告内容给到 Gitee，Gitee 将解析报告中包含的信息，并在门禁检查任务的详情上展示出来。以下为 `门禁检查报告` 的具体数据结构：

| 参数名 | 数据类型 | 参数描述 |
|---|---|---|
| title | string | 必填参数，检查任务报告的标题 |
| summary | string | 必填参数，检查项的总结概述，该属性支持 `Markdown` 格式 |
| text | string | 选填，检查项详细内容。该属性支持 `Markdown` 格式 |
| annotations | array of objects | 选填，用于将分析结果声明并注释到对应的代码上。具体数据结构参照下方 `分析注释数据结构`。 |
| images | array of objects | 选填，在报告中添加图像，图片将会展示在 UI 界面中。具体数据结构参照下方 `分析注释数据结构`。 |


###### 门禁检查报告 annotations 属性数据结构

门禁检查报告中，`output` 字段的 `annotations` 属性，可以通过声明的方式，将分析信息以注释的形式标注到代码行中，让用户可以在看代码的同时看到详细的分析结果。

> 标注后的注释可以在 `Pull Request 详情 > 文件`选项卡看到。 

| 参数名 | 数据类型 | 参数描述 |
|---|---|---|
| path | string | 必填参数，指定标注的文件路径，如：`README.md`、`src/main/example.java`|
| start_line | integer | 必填参数，注释的起始行号 |
| end_line | integer | 必填参数，注释的结束行号 |
| start_column | integer | 选填，注释起始列数，当 `start_line` 与 `end_line` 不相等时省略 |
| end_column | integer | 选填，注释结束列数，当 `start_line` 与 `end_line` 不相等时省略 |
| annotation_level | string | 选填，注释的等级。具体值可以是以下值的其中一个：<br> - `notice`: 提醒 <br>- `warning`: 警告 <br>- `failure`：失败错误 |
| message | string | 必填参数，对这几行代码的反馈的简短描述 |
| title | string | 选填，注释的标题 |
| raw_details | string | 选填，注释的详细信息 |

###### 门禁检查报告 images 属性数据结构

门禁检查报告中，`output` 字段的 `images` 属性，可以通过声明的方式，将图片添加到报告中，图片将会展示在 UI 界面中。

| 参数名 | 数据类型 | 参数描述 |
| --------- | --------- | -------------- |
| alt | string | 必填参数，图片名 |
| image_url | string | 必填参数，图片地址 |
| caption | string | 图片描述 |

###### 门禁检查任务扩展操作(actions)数据结构

在门禁检查任务中，第三方服务可以通过定义 `actions` 属性，实现在门禁检查相关界面上显示一个按钮，用于在门禁检查任务详情中，被动触发与检查项所在服务之间的交互。以下为 `门禁检查任务扩展操作` 的具体数据结构：

| 参数名 | 数据类型 | 参数描述 |
|---|---|---|
| label | string | 必填参数，展示在 UI 界面上的标签 |
| description | string | 必填参数，描述具体操作的描述。 |
| identifier | string | 必填参数，扩展操作的唯一标识 |

#### 获取某个检查任务

> `GET` /repos/{owner}/{repo}/check-runs/{check_run_id}

| 参数名 | 数据类型 | 参数描述 |
|---|---|---|
| owner | string | 必填，仓库所在的空间地址，以 https://gitee.com/oschina/git-osc 为例，`repo` 取值为 oschina |
| repo | string | 必填，仓库地址，以 https://gitee.com/oschina/git-osc 为例，`repo` 取值为 git-osc |
| check_run_id | integer | 调用 `创建一个检查任务` 接口并成功创建检查任务后返回的检查项 ID |

#### 更新某个检查任务

`PATCH` /repos/{owner}/{repo}/check-runs/{check_run_id}


| 参数名 | 数据类型 | 参数所在位置 | 参数描述 |
|---|---|---|---|
| owner | string | path | 仓库所在的空间地址，以 https://gitee.com/oschina/git-osc 为例，`repo` 取值为 oschina |
| repo | string | path | 仓库地址，以 https://gitee.com/oschina/git-osc 为例，`repo` 取值为 git-osc |
| name | string | formData | 必填参数，检查任务名称，如：ci-pipeline-runn |
| check_run_id | integer | formData | 调用 `创建一个检查任务` 接口并成功创建检查任务后返回的检查项 ID |
| details_url | string | formData | 门禁服务提供的链接 |
| status | string | formData | 当前状态。值域 queued, in_progress, completed. 默认: queued |
| started_at | string | formData | 检查任务开始时间，要求时间格式为 ISO 8601 |
| completed_at | string | formData | 检查任务完成时间，要求时间格式为 ISO 8601 |
| conclusion | string | formData | 当 completed_at 不为空或者状态是 completed 时必填参数，检查任务最终的结论。值域为：action_required, cancelled, failure, neutral, success, skipped, stale, timed_out。 当值为 action_required 时，应在 details_url 提供详情 url。 注意：提供了结论将自动设置该检查任务为 completed 状态。将再也无法更改该结论 |
| output | object | formData | 检测报告，可以接受输出各种数据，具体数据结构参照 `创建一个检查任务`->`检查任务报告属性`
 |
| actions | objects | formData | 在 Gitee 界面上显示一个按钮，具体数据结构参照 `创建一个检查任务`->`检查任务报告属性` |


#### 获取某个检查任务注释

> `GET`：/repos/{owner}/{repo}/check-runs/{check_run_id}/annotations

| 参数名 | 数据类型 | 参数描述 |
|---|---|---|
| owner | string | path | 仓库所在的空间地址，以 https://gitee.com/oschina/git-osc 为例，`repo` 取值为 oschina |
| repo | string | path | 仓库地址，以 https://gitee.com/oschina/git-osc 为例，`repo` 取值为 git-osc |
| check_run_id | integer | 调用 `创建一个检查任务` 接口并成功创建检查任务后返回的检查项 ID |
| per_page | integer | 可选参数，单次请求获取的最大数据量，最大值为 100，默认值为 20 |
| page | integer | 可选参数，请求的页数，默认值为 1 |

#### 获取指定仓库上某个提交对应的检查任务

> `GET` /repos/{owner}/{repo}/commits/{ref}/check-runs

| 参数名 | 数据类型 | 参数描述 |
|---|---|---|
| owner | string | path | 仓库所在的空间地址，以 https://gitee.com/oschina/git-osc 为例，`repo` 取值为 oschina |
| repo | string | path | 仓库地址，以 https://gitee.com/oschina/git-osc 为例，`repo` 取值为 git-osc |
| ref | string | 指定仓库上指定提交的 Commit SHA |
| check_name | string | Returns check runs with the specified name. |
| status | string | |
| per_page | integer | |
| page | integer | |

### Pull Request 检查 WebHook 数据结构说明

### WebHook 数据结构

当第三方服务在调用门禁检查项相关接口时，平台会通过 `WebHook` 将相关事件异步通知到第三方服务。具体 `WebHook` 通知数据结构如下：

| WebHook 字段 | 类型 | 字段描述 |
|---|---|---|
| action | string | 该字段表示当前用户触发的事件类型，具体值可以是以下值的其中一个：<br> - `created`: 创建检查任务<br> - `completed`: 当检查任务的状态变成完成<br> - `rerequested`: 当用户在 UI 界面上重新运行检查任务<br> - `requested_action`: 用户使用检查项提供的功能按钮 |
| requested_action | string | 第三方门禁检查服务在创建检查任务时，在门禁检查任务扩展操作(`actions`)中自定义的唯一标识 `identifier` 的值 |
| check_run | object | 被触发检查任务的详细信息 |
| project | object | 被触发检查任务所在仓库信息 |
| sender | object | 触发检查任务操作的用户信息 |


### 保护分支设置 Pull Request 准入规则

在保护分支策略里，设置「要求门禁状态成功才能合并」，勾选检查项，点击保存。在后续 Pull Request 创建检查时，如果设置「要求门禁状态成功才能合并」的检查项没有成功，该 Pull Request 是不允许合并的。

有两个注意事项：

1. 最近一周执行过的检查项才能被筛选
2. 如果设置「要求门禁状态成功才能合并」的检查项没有成功，仓库管理员可以跳过这个限制强制合并 Pull Request

快来体验吧！



